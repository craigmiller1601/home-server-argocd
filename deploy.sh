#!/bin/bash

NAMESPACE=argocd
CHART=home-server-argocd

get_helm_command() {
  override="$1"
  exists=$(helm list -n "$NAMESPACE" | grep "$CHART")

  if [ "$override" != "" ]; then
    echo "$override"
  elif [ "$exists" == "" ]; then
    echo "install"
  else
    echo "upgrade"
  fi
}

deploy() {
  helm_command=$(get_helm_command "$1")

  echo "Deploying with $helm_command command"
  helm "$helm_command" \
    -n "$NAMESPACE" \
    "$CHART" \
    ./chart \
    --values ./chart/values.yaml \
    --values ./chart/secret-values.yaml \
    --values ./chart/application-list-values.yaml \
    --wait \
    --timeout 5m
}

deploy "$@"