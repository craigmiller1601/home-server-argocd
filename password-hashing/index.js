import bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

const hashPassword = async (rawPassword) => {
    const salt = await bcrypt.genSalt(SALT_ROUNDS);
    return bcrypt.hash(rawPassword, salt);
};

hashPassword(process.argv[2])
    .then((res) => console.log(res))
    .catch((ex) => console.error(ex));