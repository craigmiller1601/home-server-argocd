# Home Server ArgoCD

This is the configuration for the ArgoCD instance running on my home server.

## Setup

### Create Namespace

```bash
kubectl create ns argocd
```

### Add Helm Repository

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
```

### Update Dependencies in Chart

```bash
cd chart
helm dependency update
```

## Deploying

Run this command:

```bash
bash deploy.sh
```

This will install or upgrade the helm chart.

## Admin Password

The admin credentials are in 1Password, under the "ArgoCD Admin" item. The password needs to be supplied as a BCrypt hashed value. For added security, it is only supplied in the git-ignored `secret-values.yaml` file.

First, provide the password as an argument to the `password-hashing` program, like this:

```bash
cd password-hashing
npm run start THE_PASSWORD
```

The output of this program will be the BCrypt version of the password.

Then, put the following in `./chart/secret-values.yaml`:

```yaml
argo-cd:
  configs:
    secret:
      argocdServerAdminPassword: BCRYPT_HASH_OF_PASSWORD
```

Lastly, deploy the changes and then restart all deployments with this command:

```bash
kubectl -n argocd rollout restart deploy
```

## Adding Applications

Applications are added via the `chart/application-list-values.yaml` file. It is organized into groups of namespaces and applications. Build it out as-needed.